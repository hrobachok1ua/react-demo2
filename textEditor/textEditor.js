import React, { Component } from 'react';

import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Typography from '@material-ui/core/Typography';

import "./textEditor.css"

class TextEditor extends Component {

    render() {
        return (
            <div className="textEditor">
                <Typography variant="h5" component="h2">Edit and add new Sos post</Typography>
                <CKEditor
                    editor={ClassicEditor}
                    data="<p>Start add post content!</p>"
                    config={{
                        toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'undo', 'redo'],
                        height: ['500px']
                    }}
                    onInit={editor => {
                        // You can store the "editor" and use when it is needed.
                        console.log('Editor is ready to use!', editor);
                    }}
                    onChange={(event, editor) => {
                        const data = editor.getData();
                        console.log({ event, editor, data });
                    }}
                    onBlur={editor => {
                        console.log('Blur.', editor);
                    }}
                    onFocus={editor => {
                        console.log('Focus.', editor);
                    }}
                />
            </div>
        )
    }
}

export default TextEditor; 