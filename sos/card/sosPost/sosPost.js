import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import "./sosPost.css"

class SosPost extends Component {

  render() {
    return (
      <Card className="card">
        <CardContent>
          <Typography color="textSecondary" gutterBottom>
            {this.props.title}
        </Typography>
          <Typography variant="h5" component="h2">
          {this.props.text}
        </Typography>
        </CardContent>
      </Card>
    )
  }
}

export default SosPost; 