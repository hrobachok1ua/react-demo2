import React, { Component } from 'react';
import './card.css'

import SosPost from "./sosPost/sosPost"

import Button from '@material-ui/core/Button';
import Collapse from '@material-ui/core/Collapse';

class Card extends Component {

  constructor() {
    super()
    this.state = {
      showSosList: true,
      cards: [{ id: "1", title: "First", text: "Один из наиболее эффективных способов помощи - поддержать материально.Ваши пожертвования в полном объеме расходуются на содержание собак и кошек Центра помощи бездомным животным, в том числе на обеспечение полного цикла лечения животных, а также ухода за тяжелобольными собаками и кошками." },
      { id: "2", title: "Second", text: "Мы уверены, что Ваше время намного важнее счетов" },
      { id: "3", title: "Third", text: "Вы не можете помочь каждому животному, но Вы можете осчастливить хотя бы одну кошку или одну собаку ! Подарите им свою поддержку !" }]

    }
  }
  showSosListHandle = () => {
    this.setState(prevState => ({
      showSosList: !prevState.showSosList,
      cards: [{ id: "1", title: "First", text: "Один из наиболее эффективных способов помощи - поддержать материально.Ваши пожертвования в полном объеме расходуются на содержание собак и кошек Центра помощи бездомным животным, в том числе на обеспечение полного цикла лечения животных, а также ухода за тяжелобольными собаками и кошками." },
      { id: "2", title: "Second", text: "Мы уверены, что Ваше время намного важнее счетов" },
      { id: "3", title: "Third", text: "Вы не можете помочь каждому животному, но Вы можете осчастливить хотя бы одну кошку или одну собаку ! Подарите им свою поддержку !" }]

    }));
  }

  render() {
    let sh = null

    if (this.props.showBtn) {
      sh = <div><Button onClick={this.showSosListHandle} variant="contained">
        We need...
    </Button>
        <Collapse in={this.state.showSosList}>
          {this.state.cards.map(el => <SosPost text={el.text} title={el.title} key={el.id}/>)}
        </Collapse>
      </div>
    }

    return (
      <div className="card">
        <img src={require('./images/card-' + this.props.imgSrc + '.png')} alt="card" className="image" />
        <div className="text">{this.props.text}
          <div className="postList">{sh}</div>
        </div>
      </div>
    )
  }
}

export default Card; 