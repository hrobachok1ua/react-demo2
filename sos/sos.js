import React, { Component } from 'react';
import './sos.css'
import LandingCard from "./landingCard/landingCard"
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { List } from '@material-ui/core';

class Sos extends Component{

    state = {
        cards: [{id: "1", showBtn: false, text: "Один из наиболее эффективных способов помощи - поддержать материально.Ваши пожертвования в полном объеме расходуются на содержание собак и кошек Центра помощи бездомным животным, в том числе на обеспечение полного цикла лечения животных, а также ухода за тяжелобольными собаками и кошками."},
                {id: "2", showBtn: true, text: "Мы уверены, что Ваше время намного важнее счетов"},
                {id: "3", showBtn: false, text: "Вы не можете помочь каждому животному, но Вы можете осчастливить хотя бы одну кошку или одну собаку ! Подарите им свою поддержку !"}]
    }

    render() {
      return (
        <div className="container">
            <Typography variant="h2">How can you help us</Typography>
            <Divider variant="middle" component="Typography"/>
            <Typography variant="h3">We are open for any help</Typography>
                <List>
                    {this.state.cards.map(el=> <LandingCard text={el.text} key={el.id}
                    imgSrc={el.id} showBtn={el.showBtn}
                    />)}
                </List>
            <Button variant="contained">All ways for helping</Button>
        </div>
      )
    }
  }

export default Sos;